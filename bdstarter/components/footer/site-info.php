<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'bdstarter' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'bdstarter' ), 'WordPress' ); ?></a>
	<span class="sep"> | </span>
	<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'bdstarter' ), 'BigDog Digital', '<a href="http://automattic.com/" rel="designer">Automattic</a>' ); ?>
</div><!-- .site-info -->