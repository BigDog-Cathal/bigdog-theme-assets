<?php

 /**
 * Enqueue scripts and styles.
 */
function bdstarter_scripts() {

	/**
	* Style.css includes all bootstrap and font awesome components, via SASS folders
	*/
	wp_enqueue_style( 'bdstarter-style', get_stylesheet_uri() );

	/**
	* Enqueue additional styles below here
	*/

	/**
	* Enqueue core scripts below here
	*/
	wp_enqueue_script( 'bdstarter-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	wp_enqueue_script( 'bdstarter-jQuery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '3.1.0', false );
	
	wp_enqueue_script( 'bdstarter-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('bdstarter-jQuery'), '20160727', true );
	
	wp_enqueue_script( 'bdstarter-global-js', get_template_directory_uri() . '/assets/js/global.js', array('bdstarter-jQuery'), '20160211', true );

	/**
	* Enqueue non-core & page specific scripts below here
	*/


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bdstarter_scripts' );

add_action('wp_print_styles', 'load_fonts');