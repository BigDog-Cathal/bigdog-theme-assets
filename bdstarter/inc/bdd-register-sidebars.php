<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bdstarter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Standard Sidebar', 'bdstarter' ),
		'id'            => 'standard-sidebar',
		'description'   => 'This sidebar is displayed on all pages that use the Default Page Template.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	
}
add_action( 'widgets_init', 'bdstarter_widgets_init' );