<?php
/**
* Register ACF theme options page
*
*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' 		=> 'dashicons-admin-settings',
		'position'		=> 59
	));

	/**
	* Add additional subpage as required below here
	*/
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Subpage Page Title',
	// 	'menu_title'	=> 'Subpage Menu Title',
	// 	'parent_slug'	=> 'subpage-slug',
	// ));
}